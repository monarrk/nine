# 9
unix shell

### building
this project depends on

- GNU Readline
- [cabal](https://www.haskell.org/cabal/)

once you have obtained this, building should be as easy as `cabal build`.

### usage
see the man page
