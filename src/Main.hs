module Main where

import Core.Init
import Core.Interpret
import Core.Terminate
import Core.Run

import Control.Exception as E
import Control.Concurrent
import System.Posix.Signals
import System.IO
import System.IO.Unsafe
import System.Console.Readline
import System.Console.CmdArgs.Explicit
import System.Environment
import System.Exit

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> do
            tid <- myThreadId
            installHandler keyboardSignal (Catch (throwTo tid UserInterrupt)) Nothing
            store <- initNine
            _ <- interpret store
            exitNine 0
        [f] -> do
            _ <- runFile f
            return ()
