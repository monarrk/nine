module Core.Init
    (initNine
    ) where

import System.Process
import System.Exit
import System.Environment
import System.IO
import Control.Exception

import Core.Interpret
import Core.Run

initNine :: IO Store
initNine = do
    user <- catch (getEnv "USER")
        (\e -> do
            let err = show (e :: IOException)
            return "user")

    putStr $ "new session from " ++ user ++ "@"
    hFlush stdout

    host <- catch (rawSystem "hostname" [])
        (\e -> do
            let err = show (e :: IOException)
            return $ ExitFailure 1)

    home <- getEnv "HOME"
    store <- runFile $ home ++ "/.9/rc.9"
    return store
