module Core
    (initNine
     , interpret
     , exitNine
    ) where

import Interpret
import Init
import Terminate
