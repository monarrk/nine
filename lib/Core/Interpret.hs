{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ViewPatterns #-}

module Core.Interpret
    (exec
     , expand
     , prompt
     , Store
    ) where

import Data.List
import Data.List (intercalate)
import Data.List.Split
import Data.Function
import System.Exit
import System.IO (hFlush, stdout, stderr, hPutStrLn)
import System.IO.Unsafe
import System.Console.Readline
import System.Process
import System.Environment
import System.Directory
import System.Console.CmdArgs.Explicit
import Control.Monad
import Control.Exception

import Core.Util

type Store = [(String, String)]

-- | display a store
showStore :: Store -> IO ()
showStore store = putStrLn $ show store

-- | the prompt before a command
prompt :: IO String
prompt = do
    maybePrompt <- lookupEnv "9PROMPT"
    case maybePrompt of
        Nothing -> return "% "
        Just prompt -> return prompt

-- | write the prompt
writePrompt :: IO String -> IO ()
writePrompt prompt = do
    p <- prompt
    putStr p >> hFlush stdout

-- | execute a command
exec :: [String] -> Store -> IO Store

-- | a comment
exec ((stripPrefix "#" -> Just name):_) s = return s

-- | set an env variable
exec ["export!", name, "=", val] s = do
    setEnv name val
    return s

-- | make a macro
exec ["define!", name, "=", val] s = return $ (name, val) : s

exec ["undefine!", name] s = return $ filter (\a -> (fst a) == name) s

-- | change directory to $HOME
exec ["cd"] s = do
    home <- getEnv "HOME"
    setCurrentDirectory home
    return s

-- | change current directory
exec ["cd", d] s = do
    setCurrentDirectory d
    return s

-- | too many args
exec ("cd":_) s = do
    printRed "Args error"
    return s

-- | an empty line
exec [] s    = return s
exec [""]  s = return s

-- | execute a command
exec s store
    -- | s == ["exit"] = return store
    | s == ["show-store"] = do; showStore store; return store
    | otherwise
    = do
        ret <-rawSystem first args
        case ret of
            ExitSuccess -> return store
            f@(ExitFailure _) -> do
                printRed $ "[9] Error: " ++ show f
                return store
    where cmd = intercalate " " s
          end = tail s
          -- make sure the first arg is only one word
          split = (splitArgs $ head s) ++ end
          first = head split
          args = tail split

-- | expand certain tokens if they need to be expanded
expand :: String -> Store -> IO String
expand (stripPrefix "\\$" -> Just name) _ = return $ "$" ++ name -- an escaped variable
expand (stripPrefix "$" -> Just name) _ = do
    maybeName <- lookupEnv name
    case maybeName of
        Nothing -> return ""
        Just var -> return var
expand (stripPrefix "~" -> Just rest) _ = do
    home <- getEnv "HOME"
    return $ home ++ rest
expand s store 
    | filtered == [] = do return s
    | otherwise
    = do
        let key = filtered!!0 in if s == (fst key) then return (snd key) else return s
    where filtered = filter (\a -> (fst a) == s) store
