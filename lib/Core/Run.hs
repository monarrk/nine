module Core.Run
    (interpret
     , runFile
     , execLine
     , run
     ) where

import Core.Interpret
import Control.Exception
import System.Console.Readline
import System.IO
import System.IO.Unsafe
import System.Exit
import System.Console.CmdArgs.Explicit

interpret :: Store -> IO Store
interpret s = do
    p <- prompt
    maybeLine <- readline p
    case maybeLine of
        Nothing     -> return [] -- EOF
        Just "exit" -> return [] -- quit
        Just line   -> do
            addHistory line
            store <- catch (run line s)
                (\e -> do
                    let err = show (e :: IOException)
                    putStrLn $ "9: error: " ++ err
                    return s)
            interpret store

runFile :: String -> IO Store
runFile s = do
    file <- catch (openFile s ReadMode)
        (\e -> do
            let err = show (e :: IOException)
            putStrLn $ "error reading file: " ++ err
            exitWith (ExitFailure 1))
    store <- execLine file []
    hClose file
    return store

execLine :: Handle -> Store -> IO Store
execLine file s = do
    eof <- hIsEOF file
    if eof then return [] else do
        inpStr <- hGetLine file
        store <- run inpStr s
        new <- execLine file store
        return $ store ++ new


run :: String -> Store -> IO Store
run line s =
    let
        toks = splitArgs line
        expanded = unsafePerformIO $ mapM (\a -> expand a s) toks
    in exec expanded s

