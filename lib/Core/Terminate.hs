module Core.Terminate
    (exitNine
    ) where

import System.Exit

exitNine :: Int -> IO a
exitNine code = case code of
    0 -> exitSuccess
    _ -> (exitWith (ExitFailure code))
