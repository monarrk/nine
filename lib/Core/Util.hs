{-# LANGUAGE OverloadedStrings #-}

module Core.Util
    (printRed
    ) where

import Rainbow
import Data.Function ((&))
import Data.Text

printRed :: String -> IO ()
printRed s = putChunksLn [ (chunk (pack s)) & (fore red) ]
